const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const mongoose = require("mongoose");

const app = express();

app.use(express.static("public"));
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({
    extended:true
}));

mongoose.connect("mongodb://localhost:27017/userDB"); 

const userSchema = {
    login: String,
    password: String
};

const User = new mongoose.model("User", userSchema);

const equipSchema = {
    type: String,
    manufacturer: String,
    qrcode: String,
    comment: String
};

const Equip = new mongoose.model("Equip", equipSchema);



app.get("/", function(req, res){
    res.render("home");
});

app.get("/login", function(req, res){
    res.render("login");
});

app.get("/priemka", function(req, res){
    console.log('print priemka page');
    res.render("priemka");
});

app.post("/priemka", function(req, res){
    const newEquip = new Equip({
        type: req.body.type,
        manufacturer: req.body.manufacturer,
        qrcode: req.body.qrcode,
        comment: req.body.comment
    });
    newEquip.save(function(err){
        if (err) {
            console.log(err);
        }else {
            res.render("home");
        }
    });
});

app.get("/register", function(req, res){
    res.render("register");
});
app.post("/register", function(req, res){
    const newUser = new User({
        login: req.body.username,
        password: req.body.password
    });
    newUser.save(function(err){
        if (err) {
            console.log(err);
        }else {
            res.render("login");
        }
    });
});



app.post("/login", function(req, res){
    const username = req.body.username;
    const password = req.body.password;

    User.findOne({login: username}, function(err, foundUser){
        if (err) {
            console.log(err);
        }else {
            if (foundUser.login === "priemka") {
                if (foundUser.password === password ) {
                    res.render("priemka");
                }
            }if (foundUser.login === "test") {
                if (foundUser.password === password ) {
                    res.render("test");
                }
            }if (foundUser.login === "sale") {
                if (foundUser.password === password ) {
                    res.render("sale");
                }
            }if (foundUser.login === "remont") {
                if (foundUser.password === password ) {
                    res.render("service");
                }
            }if (foundUser.login === "sklad") {
                if (foundUser.password === password ) {
                    res.render("sklad");
                }
            }
        }
    })
});







app.listen(3000,function(){
    console.log("Server started on port 3000.");
});


//localhost:8000/api/item